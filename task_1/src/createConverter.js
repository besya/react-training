import Converter from './Converter';

export default function createConverter(conversion){
    const { from, to } = conversion;
    return new Converter(from, to);
}