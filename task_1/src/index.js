import createConverter from './createConverter';

let conversion = { from: 'km', to: 'm' };
let converter = createConverter(conversion);
console.log(converter.convert(['2', '3'])); // should be [ '2000', '3000' ]

conversion = { from: 'C', to: 'F' };
converter = createConverter(conversion);
console.log(converter.convert(21, 1));  // should be [ 69.8, 33.8 ]

conversion = { from: 'g', to: 't' };
converter = createConverter(conversion);
console.log(converter.convert(25000)); // should be 0.025