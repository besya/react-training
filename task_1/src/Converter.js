import _ from 'lodash';


const WeightTable = { kg: 1, g: 1000, mg: 1000000, t: 0.001 };
const LengthTable = { m: 1, cm: 100, mm: 1000, km: 0.001 };
const TemperatureTable = { 
    C: (c) => (c * 1.8 + 32).toFixed(1), 
    F: (f) => ((f - 32) / 1.8).toFixed(1) 
};

export default class Converter {

    static converters = {
        weight:         { method: Converter.simpleConvert, table: WeightTable },
        length:         { method: Converter.simpleConvert, table: LengthTable },
        temperature:    { method: Converter.lambdaConvert, table: TemperatureTable }
    } 

    static simpleConvert(table, from, to, value) {
        return (value / table[from]) * table[to];
    }

    static lambdaConvert(table, from, to, value) {
        return table[this.from](value);
    }

    constructor(from, to){
        this.from = from;
        this.to = to;
        this.converter = this.getConverter();
    }

    convert(...values){
        return this.convertDynamic(values);
    }

    convertDynamic(values){
        if(values.length > 1) 
            return values.map(this.runConverter.bind(this));

        if(Array.isArray(values[0]))
            return this.convertDynamic(values[0]);

        return this.runConverter(values[0]);
    }

    runConverter(value){
        let method = this.converter.method.bind(this);
        let table = this.converter.table;
        let result = method(table, this.from, this.to, value);
        return this.toSourceType(value, result);
    }
    
    getConverter(){
        return _.find(Converter.converters, c => this.isTable(c.table));
    }

    isTable(table){
        return (this.from in table && this.to in table) ? true : false;
    }

    toSourceType(value, result){
        return (typeof value == 'string') ? result.toString() : parseFloat(result);
    }
}